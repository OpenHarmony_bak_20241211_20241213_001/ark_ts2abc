{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSTupleType",
              "elementTypes": [
                {
                  "type": "TSNamedTupleMember",
                  "elementType": {
                    "type": "TSTupleType",
                    "elementTypes": [
                      {
                        "type": "TSNamedTupleMember",
                        "elementType": {
                          "type": "TSNumberKeyword",
                          "loc": {
                            "start": {
                              "line": 2,
                              "column": 17
                            },
                            "end": {
                              "line": 2,
                              "column": 23
                            }
                          }
                        },
                        "label": {
                          "type": "Identifier",
                          "name": "a",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 2,
                              "column": 14
                            },
                            "end": {
                              "line": 2,
                              "column": 15
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 2,
                            "column": 14
                          },
                          "end": {
                            "line": 2,
                            "column": 24
                          }
                        }
                      },
                      {
                        "type": "TSNamedTupleMember",
                        "elementType": {
                          "type": "TSNumberKeyword",
                          "loc": {
                            "start": {
                              "line": 2,
                              "column": 29
                            },
                            "end": {
                              "line": 2,
                              "column": 35
                            }
                          }
                        },
                        "label": {
                          "type": "Identifier",
                          "name": "b",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 2,
                              "column": 25
                            },
                            "end": {
                              "line": 2,
                              "column": 26
                            }
                          }
                        },
                        "optional": true,
                        "loc": {
                          "start": {
                            "line": 2,
                            "column": 25
                          },
                          "end": {
                            "line": 2,
                            "column": 36
                          }
                        }
                      }
                    ],
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 13
                      },
                      "end": {
                        "line": 2,
                        "column": 36
                      }
                    }
                  },
                  "label": {
                    "type": "Identifier",
                    "name": "a",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 9
                      },
                      "end": {
                        "line": 2,
                        "column": 10
                      }
                    }
                  },
                  "optional": true,
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 9
                    },
                    "end": {
                      "line": 2,
                      "column": 37
                    }
                  }
                },
                {
                  "type": "TSNamedTupleMember",
                  "elementType": {
                    "type": "TSTupleType",
                    "elementTypes": [
                      {
                        "type": "TSUnionType",
                        "types": [
                          {
                            "type": "TSNumberKeyword",
                            "loc": {
                              "start": {
                                "line": 2,
                                "column": 43
                              },
                              "end": {
                                "line": 2,
                                "column": 49
                              }
                            }
                          },
                          {
                            "type": "TSStringKeyword",
                            "loc": {
                              "start": {
                                "line": 2,
                                "column": 52
                              },
                              "end": {
                                "line": 2,
                                "column": 58
                              }
                            }
                          }
                        ],
                        "loc": {
                          "start": {
                            "line": 2,
                            "column": 43
                          },
                          "end": {
                            "line": 2,
                            "column": 59
                          }
                        }
                      }
                    ],
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 42
                      },
                      "end": {
                        "line": 2,
                        "column": 59
                      }
                    }
                  },
                  "label": {
                    "type": "Identifier",
                    "name": "b",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 38
                      },
                      "end": {
                        "line": 2,
                        "column": 39
                      }
                    }
                  },
                  "optional": true,
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 38
                    },
                    "end": {
                      "line": 2,
                      "column": 60
                    }
                  }
                },
                {
                  "type": "TSNamedTupleMember",
                  "elementType": {
                    "type": "TSTypeQuery",
                    "exprName": {
                      "type": "Identifier",
                      "name": "b",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 2,
                          "column": 72
                        },
                        "end": {
                          "line": 2,
                          "column": 73
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 65
                      },
                      "end": {
                        "line": 2,
                        "column": 73
                      }
                    }
                  },
                  "label": {
                    "type": "Identifier",
                    "name": "c",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 61
                      },
                      "end": {
                        "line": 2,
                        "column": 62
                      }
                    }
                  },
                  "optional": true,
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 61
                    },
                    "end": {
                      "line": 2,
                      "column": 74
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 8
                },
                "end": {
                  "line": 2,
                  "column": 74
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 6
              }
            }
          },
          "init": {
            "type": "ArrayExpression",
            "elements": [
              {
                "type": "ArrayExpression",
                "elements": [
                  {
                    "type": "NumberLiteral",
                    "value": 2,
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 79
                      },
                      "end": {
                        "line": 2,
                        "column": 80
                      }
                    }
                  }
                ],
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 78
                  },
                  "end": {
                    "line": 2,
                    "column": 81
                  }
                }
              },
              {
                "type": "CallExpression",
                "callee": {
                  "type": "Identifier",
                  "name": "func",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 83
                    },
                    "end": {
                      "line": 2,
                      "column": 87
                    }
                  }
                },
                "arguments": [
                  {
                    "type": "NumberLiteral",
                    "value": 5,
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 88
                      },
                      "end": {
                        "line": 2,
                        "column": 89
                      }
                    }
                  },
                  {
                    "type": "StringLiteral",
                    "value": "",
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 91
                      },
                      "end": {
                        "line": 2,
                        "column": 96
                      }
                    }
                  }
                ],
                "optional": false,
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 83
                  },
                  "end": {
                    "line": 2,
                    "column": 97
                  }
                }
              },
              {
                "type": "ArrayExpression",
                "elements": [
                  {
                    "type": "ArrayExpression",
                    "elements": [
                      {
                        "type": "NumberLiteral",
                        "value": 1,
                        "loc": {
                          "start": {
                            "line": 2,
                            "column": 101
                          },
                          "end": {
                            "line": 2,
                            "column": 102
                          }
                        }
                      }
                    ],
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 100
                      },
                      "end": {
                        "line": 2,
                        "column": 103
                      }
                    }
                  }
                ],
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 99
                  },
                  "end": {
                    "line": 2,
                    "column": 104
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 2,
                "column": 77
              },
              "end": {
                "line": 2,
                "column": 105
              }
            }
          },
          "loc": {
            "start": {
              "line": 2,
              "column": 5
            },
            "end": {
              "line": 2,
              "column": 105
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 2,
          "column": 1
        },
        "end": {
          "line": 2,
          "column": 106
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "b",
            "typeAnnotation": {
              "type": "TSTupleType",
              "elementTypes": [
                {
                  "type": "TSTupleType",
                  "elementTypes": [
                    {
                      "type": "TSTupleType",
                      "elementTypes": [
                        {
                          "type": "TSNumberKeyword",
                          "loc": {
                            "start": {
                              "line": 3,
                              "column": 11
                            },
                            "end": {
                              "line": 3,
                              "column": 18
                            }
                          }
                        }
                      ],
                      "loc": {
                        "start": {
                          "line": 3,
                          "column": 10
                        },
                        "end": {
                          "line": 3,
                          "column": 19
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 3,
                      "column": 9
                    },
                    "end": {
                      "line": 3,
                      "column": 20
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 3,
                  "column": 8
                },
                "end": {
                  "line": 3,
                  "column": 20
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 3,
                "column": 5
              },
              "end": {
                "line": 3,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 3,
              "column": 5
            },
            "end": {
              "line": 3,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 3,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 21
        }
      }
    },
    {
      "type": "FunctionDeclaration",
      "function": {
        "type": "ScriptFunction",
        "id": {
          "type": "Identifier",
          "name": "func",
          "decorators": [],
          "loc": {
            "start": {
              "line": 5,
              "column": 10
            },
            "end": {
              "line": 5,
              "column": 14
            }
          }
        },
        "generator": false,
        "async": false,
        "expression": false,
        "params": [
          {
            "type": "Identifier",
            "name": "c",
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 5,
                  "column": 18
                },
                "end": {
                  "line": 5,
                  "column": 24
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 5,
                "column": 15
              },
              "end": {
                "line": 5,
                "column": 16
              }
            }
          },
          {
            "type": "Identifier",
            "name": "d",
            "typeAnnotation": {
              "type": "TSStringKeyword",
              "loc": {
                "start": {
                  "line": 5,
                  "column": 29
                },
                "end": {
                  "line": 5,
                  "column": 35
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 5,
                "column": 26
              },
              "end": {
                "line": 5,
                "column": 27
              }
            }
          }
        ],
        "returnType": {
          "type": "TSTupleType",
          "elementTypes": [
            {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 5,
                  "column": 39
                },
                "end": {
                  "line": 5,
                  "column": 46
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 5,
              "column": 38
            },
            "end": {
              "line": 5,
              "column": 46
            }
          }
        },
        "body": {
          "type": "BlockStatement",
          "statements": [
            {
              "type": "ReturnStatement",
              "argument": {
                "type": "ArrayExpression",
                "elements": [
                  {
                    "type": "NumberLiteral",
                    "value": 1,
                    "loc": {
                      "start": {
                        "line": 6,
                        "column": 13
                      },
                      "end": {
                        "line": 6,
                        "column": 14
                      }
                    }
                  }
                ],
                "loc": {
                  "start": {
                    "line": 6,
                    "column": 12
                  },
                  "end": {
                    "line": 6,
                    "column": 15
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 6,
                  "column": 5
                },
                "end": {
                  "line": 6,
                  "column": 16
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 5,
              "column": 47
            },
            "end": {
              "line": 7,
              "column": 2
            }
          }
        },
        "loc": {
          "start": {
            "line": 5,
            "column": 1
          },
          "end": {
            "line": 7,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 5,
          "column": 1
        },
        "end": {
          "line": 7,
          "column": 2
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 7,
      "column": 2
    }
  }
}
TypeError: Type 'number' is not assignable to type '[number]'. [tupleAssignability18.ts:2:101]
