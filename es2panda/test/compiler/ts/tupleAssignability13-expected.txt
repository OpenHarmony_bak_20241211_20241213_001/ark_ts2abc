{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSTupleType",
              "elementTypes": [
                {
                  "type": "TSNumberKeyword",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 9
                    },
                    "end": {
                      "line": 1,
                      "column": 16
                    }
                  }
                },
                {
                  "type": "TSNumberKeyword",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 17
                    },
                    "end": {
                      "line": 1,
                      "column": 24
                    }
                  }
                },
                {
                  "type": "TSStringKeyword",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 25
                    },
                    "end": {
                      "line": 1,
                      "column": 32
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 8
                },
                "end": {
                  "line": 1,
                  "column": 32
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 5
              },
              "end": {
                "line": 1,
                "column": 6
              }
            }
          },
          "init": {
            "type": "ArrayExpression",
            "elements": [
              {
                "type": "NumberLiteral",
                "value": 1,
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 36
                  },
                  "end": {
                    "line": 1,
                    "column": 37
                  }
                }
              },
              {
                "type": "NumberLiteral",
                "value": 2,
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 39
                  },
                  "end": {
                    "line": 1,
                    "column": 40
                  }
                }
              },
              {
                "type": "StringLiteral",
                "value": "",
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 42
                  },
                  "end": {
                    "line": 1,
                    "column": 47
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 1,
                "column": 35
              },
              "end": {
                "line": 1,
                "column": 48
              }
            }
          },
          "loc": {
            "start": {
              "line": 1,
              "column": 5
            },
            "end": {
              "line": 1,
              "column": 48
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 49
        }
      }
    },
    {
      "type": "FunctionDeclaration",
      "function": {
        "type": "ScriptFunction",
        "id": {
          "type": "Identifier",
          "name": "b",
          "decorators": [],
          "loc": {
            "start": {
              "line": 3,
              "column": 10
            },
            "end": {
              "line": 3,
              "column": 11
            }
          }
        },
        "generator": false,
        "async": false,
        "expression": false,
        "params": [
          {
            "type": "Identifier",
            "name": "c",
            "typeAnnotation": {
              "type": "TSTypeQuery",
              "exprName": {
                "type": "Identifier",
                "name": "a",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 3,
                    "column": 22
                  },
                  "end": {
                    "line": 3,
                    "column": 23
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 3,
                  "column": 15
                },
                "end": {
                  "line": 3,
                  "column": 23
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 3,
                "column": 12
              },
              "end": {
                "line": 3,
                "column": 13
              }
            }
          }
        ],
        "body": {
          "type": "BlockStatement",
          "statements": [],
          "loc": {
            "start": {
              "line": 3,
              "column": 25
            },
            "end": {
              "line": 5,
              "column": 2
            }
          }
        },
        "loc": {
          "start": {
            "line": 3,
            "column": 1
          },
          "end": {
            "line": 5,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 3,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 2
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "CallExpression",
        "callee": {
          "type": "Identifier",
          "name": "b",
          "decorators": [],
          "loc": {
            "start": {
              "line": 7,
              "column": 1
            },
            "end": {
              "line": 7,
              "column": 2
            }
          }
        },
        "arguments": [
          {
            "type": "ArrayExpression",
            "elements": [
              {
                "type": "NumberLiteral",
                "value": 1,
                "loc": {
                  "start": {
                    "line": 7,
                    "column": 4
                  },
                  "end": {
                    "line": 7,
                    "column": 5
                  }
                }
              },
              {
                "type": "NumberLiteral",
                "value": 2,
                "loc": {
                  "start": {
                    "line": 7,
                    "column": 7
                  },
                  "end": {
                    "line": 7,
                    "column": 8
                  }
                }
              },
              {
                "type": "NumberLiteral",
                "value": 3,
                "loc": {
                  "start": {
                    "line": 7,
                    "column": 10
                  },
                  "end": {
                    "line": 7,
                    "column": 11
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 7,
                "column": 3
              },
              "end": {
                "line": 7,
                "column": 12
              }
            }
          }
        ],
        "optional": false,
        "loc": {
          "start": {
            "line": 7,
            "column": 1
          },
          "end": {
            "line": 7,
            "column": 13
          }
        }
      },
      "loc": {
        "start": {
          "line": 7,
          "column": 1
        },
        "end": {
          "line": 7,
          "column": 14
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 7,
      "column": 14
    }
  }
}
TypeError: Type 'number' is not assignable to type 'string'. [tupleAssignability13.ts:7:10]
