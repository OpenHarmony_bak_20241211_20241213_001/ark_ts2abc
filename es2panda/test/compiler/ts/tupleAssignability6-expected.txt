{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSTupleType",
              "elementTypes": [
                {
                  "type": "TSTupleType",
                  "elementTypes": [
                    {
                      "type": "TSNamedTupleMember",
                      "elementType": {
                        "type": "TSNumberKeyword",
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 13
                          },
                          "end": {
                            "line": 1,
                            "column": 19
                          }
                        }
                      },
                      "label": {
                        "type": "Identifier",
                        "name": "a",
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 10
                          },
                          "end": {
                            "line": 1,
                            "column": 11
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 10
                        },
                        "end": {
                          "line": 1,
                          "column": 20
                        }
                      }
                    },
                    {
                      "type": "TSNamedTupleMember",
                      "elementType": {
                        "type": "TSStringKeyword",
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 25
                          },
                          "end": {
                            "line": 1,
                            "column": 31
                          }
                        }
                      },
                      "label": {
                        "type": "Identifier",
                        "name": "b",
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 21
                          },
                          "end": {
                            "line": 1,
                            "column": 22
                          }
                        }
                      },
                      "optional": true,
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 21
                        },
                        "end": {
                          "line": 1,
                          "column": 32
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 9
                    },
                    "end": {
                      "line": 1,
                      "column": 33
                    }
                  }
                },
                {
                  "type": "TSTupleType",
                  "elementTypes": [
                    {
                      "type": "TSUnionType",
                      "types": [
                        {
                          "type": "TSBooleanKeyword",
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 35
                            },
                            "end": {
                              "line": 1,
                              "column": 42
                            }
                          }
                        },
                        {
                          "type": "TSStringKeyword",
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 45
                            },
                            "end": {
                              "line": 1,
                              "column": 51
                            }
                          }
                        }
                      ],
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 35
                        },
                        "end": {
                          "line": 1,
                          "column": 52
                        }
                      }
                    },
                    {
                      "type": "TSArrayType",
                      "elementType": {
                        "type": "TSNumberKeyword",
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 53
                          },
                          "end": {
                            "line": 1,
                            "column": 59
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 53
                        },
                        "end": {
                          "line": 1,
                          "column": 62
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 34
                    },
                    "end": {
                      "line": 1,
                      "column": 63
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 8
                },
                "end": {
                  "line": 1,
                  "column": 63
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 5
              },
              "end": {
                "line": 1,
                "column": 6
              }
            }
          },
          "init": {
            "type": "ArrayExpression",
            "elements": [
              {
                "type": "ArrayExpression",
                "elements": [
                  {
                    "type": "NumberLiteral",
                    "value": 1,
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 68
                      },
                      "end": {
                        "line": 1,
                        "column": 69
                      }
                    }
                  }
                ],
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 67
                  },
                  "end": {
                    "line": 1,
                    "column": 70
                  }
                }
              },
              {
                "type": "ArrayExpression",
                "elements": [
                  {
                    "type": "StringLiteral",
                    "value": "",
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 73
                      },
                      "end": {
                        "line": 1,
                        "column": 78
                      }
                    }
                  },
                  {
                    "type": "ArrayExpression",
                    "elements": [
                      {
                        "type": "NumberLiteral",
                        "value": 1,
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 81
                          },
                          "end": {
                            "line": 1,
                            "column": 82
                          }
                        }
                      },
                      {
                        "type": "NumberLiteral",
                        "value": 2,
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 84
                          },
                          "end": {
                            "line": 1,
                            "column": 85
                          }
                        }
                      },
                      {
                        "type": "StringLiteral",
                        "value": "",
                        "loc": {
                          "start": {
                            "line": 1,
                            "column": 87
                          },
                          "end": {
                            "line": 1,
                            "column": 92
                          }
                        }
                      }
                    ],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 80
                      },
                      "end": {
                        "line": 1,
                        "column": 93
                      }
                    }
                  }
                ],
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 72
                  },
                  "end": {
                    "line": 1,
                    "column": 94
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 1,
                "column": 66
              },
              "end": {
                "line": 1,
                "column": 95
              }
            }
          },
          "loc": {
            "start": {
              "line": 1,
              "column": 5
            },
            "end": {
              "line": 1,
              "column": 95
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 96
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 1,
      "column": 96
    }
  }
}
TypeError: Type 'string' is not assignable to type 'number'. [tupleAssignability6.ts:1:87]
