{
  "type": "Program",
  "statements": [
    {
      "type": "FunctionDeclaration",
      "function": {
        "type": "ScriptFunction",
        "id": {
          "type": "Identifier",
          "name": "func",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 10
            },
            "end": {
              "line": 1,
              "column": 14
            }
          }
        },
        "generator": false,
        "async": false,
        "expression": false,
        "params": [
          {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 1,
                  "column": 18
                },
                "end": {
                  "line": 1,
                  "column": 24
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 15
              },
              "end": {
                "line": 1,
                "column": 16
              }
            }
          }
        ],
        "returnType": {
          "type": "TSNumberKeyword",
          "loc": {
            "start": {
              "line": 1,
              "column": 28
            },
            "end": {
              "line": 1,
              "column": 34
            }
          }
        },
        "body": {
          "type": "BlockStatement",
          "statements": [
            {
              "type": "ExpressionStatement",
              "expression": {
                "type": "UpdateExpression",
                "operator": "++",
                "prefix": false,
                "argument": {
                  "type": "Identifier",
                  "name": "a",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 5
                    },
                    "end": {
                      "line": 2,
                      "column": 6
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 5
                  },
                  "end": {
                    "line": 2,
                    "column": 8
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 2,
                  "column": 5
                },
                "end": {
                  "line": 2,
                  "column": 9
                }
              }
            },
            {
              "type": "ReturnStatement",
              "argument": {
                "type": "ConditionalExpression",
                "test": {
                  "type": "BinaryExpression",
                  "operator": "<",
                  "left": {
                    "type": "Identifier",
                    "name": "a",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 3,
                        "column": 12
                      },
                      "end": {
                        "line": 3,
                        "column": 13
                      }
                    }
                  },
                  "right": {
                    "type": "NumberLiteral",
                    "value": 0,
                    "loc": {
                      "start": {
                        "line": 3,
                        "column": 16
                      },
                      "end": {
                        "line": 3,
                        "column": 17
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 3,
                      "column": 12
                    },
                    "end": {
                      "line": 3,
                      "column": 17
                    }
                  }
                },
                "consequent": {
                  "type": "CallExpression",
                  "callee": {
                    "type": "Identifier",
                    "name": "func",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 3,
                        "column": 20
                      },
                      "end": {
                        "line": 3,
                        "column": 24
                      }
                    }
                  },
                  "arguments": [
                    {
                      "type": "Identifier",
                      "name": "a",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 3,
                          "column": 25
                        },
                        "end": {
                          "line": 3,
                          "column": 26
                        }
                      }
                    }
                  ],
                  "optional": false,
                  "loc": {
                    "start": {
                      "line": 3,
                      "column": 20
                    },
                    "end": {
                      "line": 3,
                      "column": 27
                    }
                  }
                },
                "alternate": {
                  "type": "Identifier",
                  "name": "a",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 3,
                      "column": 30
                    },
                    "end": {
                      "line": 3,
                      "column": 31
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 3,
                    "column": 12
                  },
                  "end": {
                    "line": 3,
                    "column": 31
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 3,
                  "column": 5
                },
                "end": {
                  "line": 3,
                  "column": 32
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 1,
              "column": 35
            },
            "end": {
              "line": 4,
              "column": 2
            }
          }
        },
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 4,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 4,
          "column": 2
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "CallExpression",
        "callee": {
          "type": "Identifier",
          "name": "func",
          "decorators": [],
          "loc": {
            "start": {
              "line": 5,
              "column": 1
            },
            "end": {
              "line": 5,
              "column": 5
            }
          }
        },
        "arguments": [
          {
            "type": "StringLiteral",
            "value": "",
            "loc": {
              "start": {
                "line": 5,
                "column": 6
              },
              "end": {
                "line": 5,
                "column": 11
              }
            }
          }
        ],
        "optional": false,
        "loc": {
          "start": {
            "line": 5,
            "column": 1
          },
          "end": {
            "line": 5,
            "column": 12
          }
        }
      },
      "loc": {
        "start": {
          "line": 5,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 13
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 5,
      "column": 13
    }
  }
}
TypeError: Argument of type 'string' is not assignable to parameter of type 'number'. [functionCall_4.ts:5:6]
