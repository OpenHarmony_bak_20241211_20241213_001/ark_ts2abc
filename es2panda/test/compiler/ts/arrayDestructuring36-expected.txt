{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 5
              },
              "end": {
                "line": 1,
                "column": 6
              }
            }
          },
          "init": {
            "type": "ObjectExpression",
            "properties": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 9
              },
              "end": {
                "line": 1,
                "column": 11
              }
            }
          },
          "loc": {
            "start": {
              "line": 1,
              "column": 5
            },
            "end": {
              "line": 1,
              "column": 11
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 12
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "b",
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 2,
                  "column": 8
                },
                "end": {
                  "line": 2,
                  "column": 14
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 2,
              "column": 5
            },
            "end": {
              "line": 2,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 2,
          "column": 1
        },
        "end": {
          "line": 2,
          "column": 15
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "c",
            "typeAnnotation": {
              "type": "TSStringKeyword",
              "loc": {
                "start": {
                  "line": 3,
                  "column": 7
                },
                "end": {
                  "line": 3,
                  "column": 13
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 3,
                "column": 5
              },
              "end": {
                "line": 3,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 3,
              "column": 5
            },
            "end": {
              "line": 3,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 3,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 14
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "d",
            "decorators": [],
            "loc": {
              "start": {
                "line": 4,
                "column": 5
              },
              "end": {
                "line": 4,
                "column": 6
              }
            }
          },
          "init": {
            "type": "ArrayExpression",
            "elements": [],
            "loc": {
              "start": {
                "line": 4,
                "column": 9
              },
              "end": {
                "line": 4,
                "column": 11
              }
            }
          },
          "loc": {
            "start": {
              "line": 4,
              "column": 5
            },
            "end": {
              "line": 4,
              "column": 11
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 4,
          "column": 1
        },
        "end": {
          "line": 4,
          "column": 12
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "e",
            "decorators": [],
            "loc": {
              "start": {
                "line": 5,
                "column": 5
              },
              "end": {
                "line": 5,
                "column": 6
              }
            }
          },
          "init": {
            "type": "TSAsExpression",
            "expression": {
              "type": "ArrayExpression",
              "elements": [
                {
                  "type": "StringLiteral",
                  "value": "",
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 10
                    },
                    "end": {
                      "line": 5,
                      "column": 15
                    }
                  }
                },
                {
                  "type": "NumberLiteral",
                  "value": 5,
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 17
                    },
                    "end": {
                      "line": 5,
                      "column": 18
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 5,
                  "column": 9
                },
                "end": {
                  "line": 5,
                  "column": 19
                }
              }
            },
            "typeAnnotation": {
              "type": "TSTypeReference",
              "typeName": {
                "type": "Identifier",
                "name": "const",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 23
                  },
                  "end": {
                    "line": 5,
                    "column": 28
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 5,
                  "column": 23
                },
                "end": {
                  "line": 5,
                  "column": 28
                }
              }
            },
            "loc": {
              "start": {
                "line": 5,
                "column": 9
              },
              "end": {
                "line": 5,
                "column": 29
              }
            }
          },
          "loc": {
            "start": {
              "line": 5,
              "column": 5
            },
            "end": {
              "line": 5,
              "column": 29
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 5,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 29
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "AssignmentExpression",
        "operator": "=",
        "left": {
          "type": "ArrayPattern",
          "elements": [
            {
              "type": "Identifier",
              "name": "a",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 6,
                  "column": 2
                },
                "end": {
                  "line": 6,
                  "column": 3
                }
              }
            },
            {
              "type": "AssignmentPattern",
              "left": {
                "type": "ArrayPattern",
                "elements": [
                  {
                    "type": "Identifier",
                    "name": "b",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 6,
                        "column": 6
                      },
                      "end": {
                        "line": 6,
                        "column": 7
                      }
                    }
                  },
                  {
                    "type": "OmittedExpression",
                    "loc": {
                      "start": {
                        "line": 6,
                        "column": 9
                      },
                      "end": {
                        "line": 6,
                        "column": 10
                      }
                    }
                  },
                  {
                    "type": "AssignmentPattern",
                    "left": {
                      "type": "Identifier",
                      "name": "c",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 6,
                          "column": 11
                        },
                        "end": {
                          "line": 6,
                          "column": 12
                        }
                      }
                    },
                    "right": {
                      "type": "StringLiteral",
                      "value": "",
                      "loc": {
                        "start": {
                          "line": 6,
                          "column": 15
                        },
                        "end": {
                          "line": 6,
                          "column": 20
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 6,
                        "column": 11
                      },
                      "end": {
                        "line": 6,
                        "column": 20
                      }
                    }
                  }
                ],
                "loc": {
                  "start": {
                    "line": 6,
                    "column": 5
                  },
                  "end": {
                    "line": 6,
                    "column": 21
                  }
                }
              },
              "right": {
                "type": "Identifier",
                "name": "e",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 6,
                    "column": 24
                  },
                  "end": {
                    "line": 6,
                    "column": 25
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 6,
                  "column": 5
                },
                "end": {
                  "line": 6,
                  "column": 25
                }
              }
            },
            {
              "type": "RestElement",
              "argument": {
                "type": "Identifier",
                "name": "d",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 6,
                    "column": 30
                  },
                  "end": {
                    "line": 6,
                    "column": 31
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 6,
                  "column": 27
                },
                "end": {
                  "line": 6,
                  "column": 31
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 6,
              "column": 1
            },
            "end": {
              "line": 6,
              "column": 32
            }
          }
        },
        "right": {
          "type": "ArrayExpression",
          "elements": [
            {
              "type": "ObjectExpression",
              "properties": [],
              "loc": {
                "start": {
                  "line": 6,
                  "column": 36
                },
                "end": {
                  "line": 6,
                  "column": 38
                }
              }
            },
            {
              "type": "ArrayExpression",
              "elements": [
                {
                  "type": "NumberLiteral",
                  "value": 42,
                  "loc": {
                    "start": {
                      "line": 6,
                      "column": 41
                    },
                    "end": {
                      "line": 6,
                      "column": 43
                    }
                  }
                },
                {
                  "type": "OmittedExpression",
                  "loc": {
                    "start": {
                      "line": 6,
                      "column": 45
                    },
                    "end": {
                      "line": 6,
                      "column": 46
                    }
                  }
                },
                {
                  "type": "BinaryExpression",
                  "operator": "+",
                  "left": {
                    "type": "StringLiteral",
                    "value": "",
                    "loc": {
                      "start": {
                        "line": 6,
                        "column": 47
                      },
                      "end": {
                        "line": 6,
                        "column": 52
                      }
                    }
                  },
                  "right": {
                    "type": "NumberLiteral",
                    "value": 2,
                    "loc": {
                      "start": {
                        "line": 6,
                        "column": 55
                      },
                      "end": {
                        "line": 6,
                        "column": 56
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 6,
                      "column": 47
                    },
                    "end": {
                      "line": 6,
                      "column": 56
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 6,
                  "column": 40
                },
                "end": {
                  "line": 6,
                  "column": 57
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 6,
              "column": 35
            },
            "end": {
              "line": 6,
              "column": 58
            }
          }
        },
        "loc": {
          "start": {
            "line": 6,
            "column": 1
          },
          "end": {
            "line": 6,
            "column": 58
          }
        }
      },
      "loc": {
        "start": {
          "line": 6,
          "column": 1
        },
        "end": {
          "line": 6,
          "column": 59
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 6,
      "column": 59
    }
  }
}
TypeError: Type '"foo"' is not assignable to type 'number'. [arrayDestructuring36.ts:6:6]
