{
  "type": "Program",
  "statements": [
    {
      "type": "FunctionDeclaration",
      "function": {
        "type": "ScriptFunction",
        "id": {
          "type": "Identifier",
          "name": "a",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 10
            },
            "end": {
              "line": 1,
              "column": 11
            }
          }
        },
        "generator": false,
        "async": false,
        "expression": false,
        "params": [],
        "returnType": {
          "type": "TSTupleType",
          "elementTypes": [
            {
              "type": "TSStringKeyword",
              "loc": {
                "start": {
                  "line": 1,
                  "column": 16
                },
                "end": {
                  "line": 1,
                  "column": 23
                }
              }
            },
            {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 1,
                  "column": 24
                },
                "end": {
                  "line": 1,
                  "column": 31
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 1,
              "column": 15
            },
            "end": {
              "line": 1,
              "column": 31
            }
          }
        },
        "body": {
          "type": "BlockStatement",
          "statements": [
            {
              "type": "ReturnStatement",
              "argument": {
                "type": "ArrayExpression",
                "elements": [
                  {
                    "type": "StringLiteral",
                    "value": "",
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 13
                      },
                      "end": {
                        "line": 2,
                        "column": 18
                      }
                    }
                  },
                  {
                    "type": "StringLiteral",
                    "value": "",
                    "loc": {
                      "start": {
                        "line": 2,
                        "column": 20
                      },
                      "end": {
                        "line": 2,
                        "column": 25
                      }
                    }
                  }
                ],
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 12
                  },
                  "end": {
                    "line": 2,
                    "column": 26
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 2,
                  "column": 5
                },
                "end": {
                  "line": 2,
                  "column": 27
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 1,
              "column": 32
            },
            "end": {
              "line": 3,
              "column": 2
            }
          }
        },
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 3,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 2
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 4,
      "column": 1
    }
  }
}
TypeError: Type 'string' is not assignable to type 'number'. [tupleAssignability10.ts:2:20]
