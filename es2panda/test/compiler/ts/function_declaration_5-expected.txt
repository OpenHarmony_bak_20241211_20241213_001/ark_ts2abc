{
  "type": "Program",
  "statements": [
    {
      "type": "FunctionDeclaration",
      "function": {
        "type": "ScriptFunction",
        "id": {
          "type": "Identifier",
          "name": "foo",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 10
            },
            "end": {
              "line": 1,
              "column": 13
            }
          }
        },
        "generator": false,
        "async": false,
        "expression": false,
        "params": [],
        "returnType": {
          "type": "TSNumberKeyword",
          "loc": {
            "start": {
              "line": 1,
              "column": 17
            },
            "end": {
              "line": 1,
              "column": 23
            }
          }
        },
        "body": {
          "type": "BlockStatement",
          "statements": [
            {
              "type": "ReturnStatement",
              "argument": {
                "type": "BooleanLiteral",
                "value": false,
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 8
                  },
                  "end": {
                    "line": 2,
                    "column": 13
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 2,
                  "column": 1
                },
                "end": {
                  "line": 2,
                  "column": 14
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 1,
              "column": 24
            },
            "end": {
              "line": 3,
              "column": 2
            }
          }
        },
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 3,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 2
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 4,
      "column": 1
    }
  }
}
TypeError: Type 'false' is not assignable to type 'number'. [function_declaration_5.ts:2:1]
