{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "foo",
            "typeAnnotation": {
              "type": "TSConstructorType",
              "params": [
                {
                  "type": "Identifier",
                  "name": "a",
                  "typeAnnotation": {
                    "type": "TSStringKeyword",
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 18
                      },
                      "end": {
                        "line": 1,
                        "column": 24
                      }
                    }
                  },
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 15
                    },
                    "end": {
                      "line": 1,
                      "column": 16
                    }
                  }
                },
                {
                  "type": "Identifier",
                  "name": "b",
                  "typeAnnotation": {
                    "type": "TSNumberKeyword",
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 29
                      },
                      "end": {
                        "line": 1,
                        "column": 35
                      }
                    }
                  },
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 26
                    },
                    "end": {
                      "line": 1,
                      "column": 27
                    }
                  }
                }
              ],
              "returnType": {
                "type": "TSAnyKeyword",
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 40
                  },
                  "end": {
                    "line": 1,
                    "column": 43
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 10
                },
                "end": {
                  "line": 1,
                  "column": 43
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 5
              },
              "end": {
                "line": 1,
                "column": 8
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 1,
              "column": 5
            },
            "end": {
              "line": 1,
              "column": 8
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 44
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 6
              }
            }
          },
          "init": {
            "type": "NewExpression",
            "callee": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 13
                },
                "end": {
                  "line": 2,
                  "column": 16
                }
              }
            },
            "arguments": [
              {
                "type": "StringLiteral",
                "value": "",
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 17
                  },
                  "end": {
                    "line": 2,
                    "column": 22
                  }
                }
              },
              {
                "type": "BooleanLiteral",
                "value": false,
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 23
                  },
                  "end": {
                    "line": 2,
                    "column": 28
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 2,
                "column": 9
              },
              "end": {
                "line": 2,
                "column": 29
              }
            }
          },
          "loc": {
            "start": {
              "line": 2,
              "column": 5
            },
            "end": {
              "line": 2,
              "column": 29
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 2,
          "column": 1
        },
        "end": {
          "line": 2,
          "column": 30
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 3,
      "column": 1
    }
  }
}
TypeError: Argument of type 'boolean' is not assignable to parameter of type 'number'. [new_expression_101.ts:2:23]
