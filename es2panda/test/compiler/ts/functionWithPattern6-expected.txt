{
  "type": "Program",
  "statements": [
    {
      "type": "FunctionDeclaration",
      "function": {
        "type": "ScriptFunction",
        "id": {
          "type": "Identifier",
          "name": "foo",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 10
            },
            "end": {
              "line": 1,
              "column": 13
            }
          }
        },
        "generator": false,
        "async": false,
        "expression": false,
        "params": [
          {
            "type": "AssignmentPattern",
            "left": {
              "type": "ObjectPattern",
              "properties": [
                {
                  "type": "Property",
                  "method": false,
                  "shorthand": true,
                  "computed": false,
                  "key": {
                    "type": "Identifier",
                    "name": "a",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 16
                      },
                      "end": {
                        "line": 1,
                        "column": 17
                      }
                    }
                  },
                  "value": {
                    "type": "Identifier",
                    "name": "a",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 16
                      },
                      "end": {
                        "line": 1,
                        "column": 17
                      }
                    }
                  },
                  "kind": "init",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 16
                    },
                    "end": {
                      "line": 1,
                      "column": 17
                    }
                  }
                },
                {
                  "type": "Property",
                  "method": false,
                  "shorthand": false,
                  "computed": false,
                  "key": {
                    "type": "Identifier",
                    "name": "b",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 19
                      },
                      "end": {
                        "line": 1,
                        "column": 20
                      }
                    }
                  },
                  "value": {
                    "type": "AssignmentPattern",
                    "left": {
                      "type": "ObjectPattern",
                      "properties": [
                        {
                          "type": "Property",
                          "method": false,
                          "shorthand": true,
                          "computed": false,
                          "key": {
                            "type": "Identifier",
                            "name": "t",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 1,
                                "column": 24
                              },
                              "end": {
                                "line": 1,
                                "column": 25
                              }
                            }
                          },
                          "value": {
                            "type": "AssignmentPattern",
                            "left": {
                              "type": "Identifier",
                              "name": "t",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 1,
                                  "column": 24
                                },
                                "end": {
                                  "line": 1,
                                  "column": 25
                                }
                              }
                            },
                            "right": {
                              "type": "StringLiteral",
                              "value": "",
                              "loc": {
                                "start": {
                                  "line": 1,
                                  "column": 28
                                },
                                "end": {
                                  "line": 1,
                                  "column": 33
                                }
                              }
                            },
                            "loc": {
                              "start": {
                                "line": 1,
                                "column": 24
                              },
                              "end": {
                                "line": 1,
                                "column": 33
                              }
                            }
                          },
                          "kind": "init",
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 24
                            },
                            "end": {
                              "line": 1,
                              "column": 33
                            }
                          }
                        }
                      ],
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 22
                        },
                        "end": {
                          "line": 1,
                          "column": 35
                        }
                      }
                    },
                    "right": {
                      "type": "ObjectExpression",
                      "properties": [
                        {
                          "type": "Property",
                          "method": false,
                          "shorthand": false,
                          "computed": false,
                          "key": {
                            "type": "Identifier",
                            "name": "t",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 1,
                                "column": 40
                              },
                              "end": {
                                "line": 1,
                                "column": 41
                              }
                            }
                          },
                          "value": {
                            "type": "NumberLiteral",
                            "value": 3,
                            "loc": {
                              "start": {
                                "line": 1,
                                "column": 43
                              },
                              "end": {
                                "line": 1,
                                "column": 44
                              }
                            }
                          },
                          "kind": "init",
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 40
                            },
                            "end": {
                              "line": 1,
                              "column": 44
                            }
                          }
                        }
                      ],
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 38
                        },
                        "end": {
                          "line": 1,
                          "column": 46
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 22
                      },
                      "end": {
                        "line": 1,
                        "column": 46
                      }
                    }
                  },
                  "kind": "init",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 19
                    },
                    "end": {
                      "line": 1,
                      "column": 46
                    }
                  }
                },
                {
                  "type": "Property",
                  "method": false,
                  "shorthand": false,
                  "computed": false,
                  "key": {
                    "type": "Identifier",
                    "name": "d",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 48
                      },
                      "end": {
                        "line": 1,
                        "column": 49
                      }
                    }
                  },
                  "value": {
                    "type": "AssignmentPattern",
                    "left": {
                      "type": "ArrayPattern",
                      "elements": [
                        {
                          "type": "AssignmentPattern",
                          "left": {
                            "type": "Identifier",
                            "name": "e",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 1,
                                "column": 52
                              },
                              "end": {
                                "line": 1,
                                "column": 53
                              }
                            }
                          },
                          "right": {
                            "type": "ObjectExpression",
                            "properties": [],
                            "loc": {
                              "start": {
                                "line": 1,
                                "column": 56
                              },
                              "end": {
                                "line": 1,
                                "column": 58
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 52
                            },
                            "end": {
                              "line": 1,
                              "column": 58
                            }
                          }
                        },
                        {
                          "type": "AssignmentPattern",
                          "left": {
                            "type": "Identifier",
                            "name": "f",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 1,
                                "column": 60
                              },
                              "end": {
                                "line": 1,
                                "column": 61
                              }
                            }
                          },
                          "right": {
                            "type": "NumberLiteral",
                            "value": 6,
                            "loc": {
                              "start": {
                                "line": 1,
                                "column": 64
                              },
                              "end": {
                                "line": 1,
                                "column": 65
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 60
                            },
                            "end": {
                              "line": 1,
                              "column": 65
                            }
                          }
                        }
                      ],
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 51
                        },
                        "end": {
                          "line": 1,
                          "column": 66
                        }
                      }
                    },
                    "right": {
                      "type": "ArrayExpression",
                      "elements": [
                        {
                          "type": "NumberLiteral",
                          "value": 3,
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 70
                            },
                            "end": {
                              "line": 1,
                              "column": 71
                            }
                          }
                        },
                        {
                          "type": "ArrayExpression",
                          "elements": [
                            {
                              "type": "StringLiteral",
                              "value": "",
                              "loc": {
                                "start": {
                                  "line": 1,
                                  "column": 74
                                },
                                "end": {
                                  "line": 1,
                                  "column": 79
                                }
                              }
                            },
                            {
                              "type": "StringLiteral",
                              "value": "",
                              "loc": {
                                "start": {
                                  "line": 1,
                                  "column": 81
                                },
                                "end": {
                                  "line": 1,
                                  "column": 86
                                }
                              }
                            }
                          ],
                          "loc": {
                            "start": {
                              "line": 1,
                              "column": 73
                            },
                            "end": {
                              "line": 1,
                              "column": 87
                            }
                          }
                        }
                      ],
                      "loc": {
                        "start": {
                          "line": 1,
                          "column": 69
                        },
                        "end": {
                          "line": 1,
                          "column": 88
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 51
                      },
                      "end": {
                        "line": 1,
                        "column": 88
                      }
                    }
                  },
                  "kind": "init",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 48
                    },
                    "end": {
                      "line": 1,
                      "column": 88
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 14
                },
                "end": {
                  "line": 1,
                  "column": 90
                }
              }
            },
            "right": {
              "type": "ObjectExpression",
              "properties": [
                {
                  "type": "Property",
                  "method": false,
                  "shorthand": false,
                  "computed": false,
                  "key": {
                    "type": "Identifier",
                    "name": "a",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 95
                      },
                      "end": {
                        "line": 1,
                        "column": 96
                      }
                    }
                  },
                  "value": {
                    "type": "BooleanLiteral",
                    "value": true,
                    "loc": {
                      "start": {
                        "line": 1,
                        "column": 98
                      },
                      "end": {
                        "line": 1,
                        "column": 102
                      }
                    }
                  },
                  "kind": "init",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 95
                    },
                    "end": {
                      "line": 1,
                      "column": 102
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 93
                },
                "end": {
                  "line": 1,
                  "column": 104
                }
              }
            },
            "loc": {
              "start": {
                "line": 1,
                "column": 14
              },
              "end": {
                "line": 1,
                "column": 104
              }
            }
          }
        ],
        "body": {
          "type": "BlockStatement",
          "statements": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 106
            },
            "end": {
              "line": 3,
              "column": 2
            }
          }
        },
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 3,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 3,
          "column": 2
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "CallExpression",
        "callee": {
          "type": "Identifier",
          "name": "foo",
          "decorators": [],
          "loc": {
            "start": {
              "line": 5,
              "column": 1
            },
            "end": {
              "line": 5,
              "column": 4
            }
          }
        },
        "arguments": [
          {
            "type": "ObjectExpression",
            "properties": [
              {
                "type": "Property",
                "method": false,
                "shorthand": false,
                "computed": false,
                "key": {
                  "type": "Identifier",
                  "name": "a",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 7
                    },
                    "end": {
                      "line": 5,
                      "column": 8
                    }
                  }
                },
                "value": {
                  "type": "BooleanLiteral",
                  "value": false,
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 10
                    },
                    "end": {
                      "line": 5,
                      "column": 15
                    }
                  }
                },
                "kind": "init",
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 7
                  },
                  "end": {
                    "line": 5,
                    "column": 15
                  }
                }
              },
              {
                "type": "Property",
                "method": false,
                "shorthand": false,
                "computed": false,
                "key": {
                  "type": "Identifier",
                  "name": "b",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 17
                    },
                    "end": {
                      "line": 5,
                      "column": 18
                    }
                  }
                },
                "value": {
                  "type": "ObjectExpression",
                  "properties": [
                    {
                      "type": "Property",
                      "method": false,
                      "shorthand": false,
                      "computed": false,
                      "key": {
                        "type": "Identifier",
                        "name": "r",
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 5,
                            "column": 22
                          },
                          "end": {
                            "line": 5,
                            "column": 23
                          }
                        }
                      },
                      "value": {
                        "type": "StringLiteral",
                        "value": "",
                        "loc": {
                          "start": {
                            "line": 5,
                            "column": 25
                          },
                          "end": {
                            "line": 5,
                            "column": 31
                          }
                        }
                      },
                      "kind": "init",
                      "loc": {
                        "start": {
                          "line": 5,
                          "column": 22
                        },
                        "end": {
                          "line": 5,
                          "column": 31
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 5,
                      "column": 20
                    },
                    "end": {
                      "line": 5,
                      "column": 33
                    }
                  }
                },
                "kind": "init",
                "loc": {
                  "start": {
                    "line": 5,
                    "column": 17
                  },
                  "end": {
                    "line": 5,
                    "column": 33
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 5,
                "column": 5
              },
              "end": {
                "line": 5,
                "column": 35
              }
            }
          }
        ],
        "optional": false,
        "loc": {
          "start": {
            "line": 5,
            "column": 1
          },
          "end": {
            "line": 5,
            "column": 36
          }
        }
      },
      "loc": {
        "start": {
          "line": 5,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 37
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 5,
      "column": 37
    }
  }
}
TypeError: Object literal may only specify known properties, and 'r' does not exist in type '{ t?: number; }' [functionWithPattern6.ts:5:22]
