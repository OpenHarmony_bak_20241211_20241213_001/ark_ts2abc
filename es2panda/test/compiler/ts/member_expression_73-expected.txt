{
  "type": "Program",
  "statements": [
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [
          {
            "type": "TSIndexSignature",
            "parameters": {
              "type": "Identifier",
              "name": "a",
              "typeAnnotation": {
                "type": "TSNumberKeyword",
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 19
                  },
                  "end": {
                    "line": 1,
                    "column": 25
                  }
                }
              },
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 17
                },
                "end": {
                  "line": 1,
                  "column": 18
                }
              }
            },
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 1,
                  "column": 27
                },
                "end": {
                  "line": 1,
                  "column": 33
                }
              }
            },
            "readonly": false,
            "loc": {
              "start": {
                "line": 1,
                "column": 16
              },
              "end": {
                "line": 1,
                "column": 34
              }
            }
          },
          {
            "type": "TSIndexSignature",
            "parameters": {
              "type": "Identifier",
              "name": "b",
              "typeAnnotation": {
                "type": "TSStringKeyword",
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 28
                  },
                  "end": {
                    "line": 2,
                    "column": 34
                  }
                }
              },
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 26
                },
                "end": {
                  "line": 2,
                  "column": 27
                }
              }
            },
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 2,
                  "column": 36
                },
                "end": {
                  "line": 2,
                  "column": 42
                }
              }
            },
            "readonly": true,
            "loc": {
              "start": {
                "line": 2,
                "column": 16
              },
              "end": {
                "line": 2,
                "column": 43
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 15
          },
          "end": {
            "line": 2,
            "column": 44
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "foo",
        "decorators": [],
        "loc": {
          "start": {
            "line": 1,
            "column": 11
          },
          "end": {
            "line": 1,
            "column": 14
          }
        }
      },
      "extends": [],
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 2,
          "column": 44
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSTypeReference",
              "typeName": {
                "type": "Identifier",
                "name": "foo",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 4,
                    "column": 8
                  },
                  "end": {
                    "line": 4,
                    "column": 11
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 4,
                  "column": 8
                },
                "end": {
                  "line": 4,
                  "column": 11
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 4,
                "column": 5
              },
              "end": {
                "line": 4,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 4,
              "column": 5
            },
            "end": {
              "line": 4,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 4,
          "column": 1
        },
        "end": {
          "line": 4,
          "column": 12
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "AssignmentExpression",
        "operator": "=",
        "left": {
          "type": "MemberExpression",
          "object": {
            "type": "Identifier",
            "name": "a",
            "decorators": [],
            "loc": {
              "start": {
                "line": 5,
                "column": 1
              },
              "end": {
                "line": 5,
                "column": 2
              }
            }
          },
          "property": {
            "type": "StringLiteral",
            "value": "",
            "loc": {
              "start": {
                "line": 5,
                "column": 3
              },
              "end": {
                "line": 5,
                "column": 8
              }
            }
          },
          "computed": true,
          "optional": false,
          "loc": {
            "start": {
              "line": 5,
              "column": 1
            },
            "end": {
              "line": 5,
              "column": 9
            }
          }
        },
        "right": {
          "type": "NumberLiteral",
          "value": 5,
          "loc": {
            "start": {
              "line": 5,
              "column": 12
            },
            "end": {
              "line": 5,
              "column": 13
            }
          }
        },
        "loc": {
          "start": {
            "line": 5,
            "column": 1
          },
          "end": {
            "line": 5,
            "column": 13
          }
        }
      },
      "loc": {
        "start": {
          "line": 5,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 14
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 6,
      "column": 1
    }
  }
}
TypeError: Index signature in type 'foo' only permits reading. [member_expression_73.ts:5:1]
