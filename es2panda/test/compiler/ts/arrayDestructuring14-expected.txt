{
  "type": "Program",
  "statements": [
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "a",
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 1,
                  "column": 8
                },
                "end": {
                  "line": 1,
                  "column": 14
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 5
              },
              "end": {
                "line": 1,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 1,
              "column": 5
            },
            "end": {
              "line": 1,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 15
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "ArrayPattern",
            "elements": [
              {
                "type": "AssignmentPattern",
                "left": {
                  "type": "Identifier",
                  "name": "b",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 6
                    },
                    "end": {
                      "line": 2,
                      "column": 7
                    }
                  }
                },
                "right": {
                  "type": "NumberLiteral",
                  "value": 6,
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 10
                    },
                    "end": {
                      "line": 2,
                      "column": 11
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 6
                  },
                  "end": {
                    "line": 2,
                    "column": 11
                  }
                }
              },
              {
                "type": "AssignmentPattern",
                "left": {
                  "type": "Identifier",
                  "name": "c",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 13
                    },
                    "end": {
                      "line": 2,
                      "column": 14
                    }
                  }
                },
                "right": {
                  "type": "NumberLiteral",
                  "value": 6,
                  "loc": {
                    "start": {
                      "line": 2,
                      "column": 17
                    },
                    "end": {
                      "line": 2,
                      "column": 18
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 13
                  },
                  "end": {
                    "line": 2,
                    "column": 18
                  }
                }
              }
            ],
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 19
              }
            }
          },
          "init": {
            "type": "Identifier",
            "name": "a",
            "decorators": [],
            "loc": {
              "start": {
                "line": 2,
                "column": 22
              },
              "end": {
                "line": 2,
                "column": 23
              }
            }
          },
          "loc": {
            "start": {
              "line": 2,
              "column": 5
            },
            "end": {
              "line": 2,
              "column": 23
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 2,
          "column": 1
        },
        "end": {
          "line": 2,
          "column": 24
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 2,
      "column": 24
    }
  }
}
TypeError: Type 'number' must have a '[Symbol.iterator]()' method that returns an interator [arrayDestructuring14.ts:2:5]
