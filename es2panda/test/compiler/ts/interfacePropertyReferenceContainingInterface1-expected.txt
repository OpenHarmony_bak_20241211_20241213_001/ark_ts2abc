{
  "type": "Program",
  "statements": [
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [
          {
            "type": "TSPropertySignature",
            "computed": false,
            "optional": false,
            "readonly": false,
            "key": {
              "type": "Identifier",
              "name": "a",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 2,
                  "column": 5
                },
                "end": {
                  "line": 2,
                  "column": 6
                }
              }
            },
            "typeAnnotation": {
              "type": "TSTypeReference",
              "typeName": {
                "type": "Identifier",
                "name": "B",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 2,
                    "column": 8
                  },
                  "end": {
                    "line": 2,
                    "column": 9
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 2,
                  "column": 8
                },
                "end": {
                  "line": 2,
                  "column": 9
                }
              }
            },
            "loc": {
              "start": {
                "line": 2,
                "column": 5
              },
              "end": {
                "line": 2,
                "column": 10
              }
            }
          },
          {
            "type": "TSPropertySignature",
            "computed": false,
            "optional": false,
            "readonly": false,
            "key": {
              "type": "Identifier",
              "name": "b",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 3,
                  "column": 5
                },
                "end": {
                  "line": 3,
                  "column": 6
                }
              }
            },
            "typeAnnotation": {
              "type": "TSTypeReference",
              "typeName": {
                "type": "Identifier",
                "name": "A",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 3,
                    "column": 8
                  },
                  "end": {
                    "line": 3,
                    "column": 9
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 3,
                  "column": 8
                },
                "end": {
                  "line": 3,
                  "column": 9
                }
              }
            },
            "loc": {
              "start": {
                "line": 3,
                "column": 5
              },
              "end": {
                "line": 3,
                "column": 10
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 13
          },
          "end": {
            "line": 4,
            "column": 2
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "A",
        "decorators": [],
        "loc": {
          "start": {
            "line": 1,
            "column": 11
          },
          "end": {
            "line": 1,
            "column": 12
          }
        }
      },
      "extends": [],
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 4,
          "column": 2
        }
      }
    },
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [
          {
            "type": "TSPropertySignature",
            "computed": false,
            "optional": false,
            "readonly": false,
            "key": {
              "type": "Identifier",
              "name": "c",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 7,
                  "column": 5
                },
                "end": {
                  "line": 7,
                  "column": 6
                }
              }
            },
            "typeAnnotation": {
              "type": "TSTypeReference",
              "typeName": {
                "type": "Identifier",
                "name": "A",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 7,
                    "column": 8
                  },
                  "end": {
                    "line": 7,
                    "column": 9
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 7,
                  "column": 8
                },
                "end": {
                  "line": 7,
                  "column": 9
                }
              }
            },
            "loc": {
              "start": {
                "line": 7,
                "column": 5
              },
              "end": {
                "line": 7,
                "column": 10
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 6,
            "column": 23
          },
          "end": {
            "line": 8,
            "column": 2
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "B",
        "decorators": [],
        "loc": {
          "start": {
            "line": 6,
            "column": 11
          },
          "end": {
            "line": 6,
            "column": 12
          }
        }
      },
      "extends": [
        {
          "type": "TSInterfaceHeritage",
          "expression": {
            "type": "Identifier",
            "name": "A",
            "decorators": [],
            "loc": {
              "start": {
                "line": 6,
                "column": 21
              },
              "end": {
                "line": 6,
                "column": 22
              }
            }
          },
          "loc": {
            "start": {
              "line": 6,
              "column": 23
            },
            "end": {
              "line": 6,
              "column": 22
            }
          }
        }
      ],
      "loc": {
        "start": {
          "line": 6,
          "column": 1
        },
        "end": {
          "line": 8,
          "column": 2
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "foo",
            "typeAnnotation": {
              "type": "TSTypeReference",
              "typeName": {
                "type": "Identifier",
                "name": "B",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 10,
                    "column": 10
                  },
                  "end": {
                    "line": 10,
                    "column": 11
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 10,
                  "column": 10
                },
                "end": {
                  "line": 10,
                  "column": 11
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 10,
                "column": 5
              },
              "end": {
                "line": 10,
                "column": 8
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 10,
              "column": 5
            },
            "end": {
              "line": 10,
              "column": 8
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 10,
          "column": 1
        },
        "end": {
          "line": 10,
          "column": 12
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "MemberExpression",
        "object": {
          "type": "MemberExpression",
          "object": {
            "type": "MemberExpression",
            "object": {
              "type": "MemberExpression",
              "object": {
                "type": "MemberExpression",
                "object": {
                  "type": "MemberExpression",
                  "object": {
                    "type": "Identifier",
                    "name": "foo",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 12,
                        "column": 1
                      },
                      "end": {
                        "line": 12,
                        "column": 4
                      }
                    }
                  },
                  "property": {
                    "type": "Identifier",
                    "name": "a",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 12,
                        "column": 5
                      },
                      "end": {
                        "line": 12,
                        "column": 6
                      }
                    }
                  },
                  "computed": false,
                  "optional": false,
                  "loc": {
                    "start": {
                      "line": 12,
                      "column": 1
                    },
                    "end": {
                      "line": 12,
                      "column": 6
                    }
                  }
                },
                "property": {
                  "type": "Identifier",
                  "name": "b",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 12,
                      "column": 7
                    },
                    "end": {
                      "line": 12,
                      "column": 8
                    }
                  }
                },
                "computed": false,
                "optional": false,
                "loc": {
                  "start": {
                    "line": 12,
                    "column": 1
                  },
                  "end": {
                    "line": 12,
                    "column": 8
                  }
                }
              },
              "property": {
                "type": "Identifier",
                "name": "b",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 12,
                    "column": 9
                  },
                  "end": {
                    "line": 12,
                    "column": 10
                  }
                }
              },
              "computed": false,
              "optional": false,
              "loc": {
                "start": {
                  "line": 12,
                  "column": 1
                },
                "end": {
                  "line": 12,
                  "column": 10
                }
              }
            },
            "property": {
              "type": "Identifier",
              "name": "a",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 12,
                  "column": 11
                },
                "end": {
                  "line": 12,
                  "column": 12
                }
              }
            },
            "computed": false,
            "optional": false,
            "loc": {
              "start": {
                "line": 12,
                "column": 1
              },
              "end": {
                "line": 12,
                "column": 12
              }
            }
          },
          "property": {
            "type": "Identifier",
            "name": "c",
            "decorators": [],
            "loc": {
              "start": {
                "line": 12,
                "column": 13
              },
              "end": {
                "line": 12,
                "column": 14
              }
            }
          },
          "computed": false,
          "optional": false,
          "loc": {
            "start": {
              "line": 12,
              "column": 1
            },
            "end": {
              "line": 12,
              "column": 14
            }
          }
        },
        "property": {
          "type": "Identifier",
          "name": "r",
          "decorators": [],
          "loc": {
            "start": {
              "line": 12,
              "column": 15
            },
            "end": {
              "line": 12,
              "column": 16
            }
          }
        },
        "computed": false,
        "optional": false,
        "loc": {
          "start": {
            "line": 12,
            "column": 1
          },
          "end": {
            "line": 12,
            "column": 16
          }
        }
      },
      "loc": {
        "start": {
          "line": 12,
          "column": 1
        },
        "end": {
          "line": 12,
          "column": 16
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 13,
      "column": 1
    }
  }
}
TypeError: Property 'r' does not exist on type 'A'. [interfacePropertyReferenceContainingInterface1.ts:12:15]
