{
  "type": "Program",
  "statements": [
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [
          {
            "type": "TSPropertySignature",
            "computed": false,
            "optional": false,
            "readonly": false,
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 3,
                  "column": 1
                },
                "end": {
                  "line": 3,
                  "column": 4
                }
              }
            },
            "typeAnnotation": {
              "type": "TSNumberKeyword",
              "loc": {
                "start": {
                  "line": 3,
                  "column": 6
                },
                "end": {
                  "line": 3,
                  "column": 12
                }
              }
            },
            "loc": {
              "start": {
                "line": 3,
                "column": 1
              },
              "end": {
                "line": 3,
                "column": 13
              }
            }
          },
          {
            "type": "TSPropertySignature",
            "computed": false,
            "optional": false,
            "readonly": false,
            "key": {
              "type": "Identifier",
              "name": "bar",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 4,
                  "column": 1
                },
                "end": {
                  "line": 4,
                  "column": 4
                }
              }
            },
            "typeAnnotation": {
              "type": "TSStringKeyword",
              "loc": {
                "start": {
                  "line": 4,
                  "column": 6
                },
                "end": {
                  "line": 4,
                  "column": 12
                }
              }
            },
            "loc": {
              "start": {
                "line": 4,
                "column": 1
              },
              "end": {
                "line": 5,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 2,
            "column": 13
          },
          "end": {
            "line": 5,
            "column": 2
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "a",
        "decorators": [],
        "loc": {
          "start": {
            "line": 2,
            "column": 11
          },
          "end": {
            "line": 2,
            "column": 12
          }
        }
      },
      "extends": [],
      "loc": {
        "start": {
          "line": 2,
          "column": 1
        },
        "end": {
          "line": 5,
          "column": 2
        }
      }
    },
    {
      "type": "VariableDeclaration",
      "declarations": [
        {
          "type": "VariableDeclarator",
          "id": {
            "type": "Identifier",
            "name": "b",
            "typeAnnotation": {
              "type": "TSTypeReference",
              "typeName": {
                "type": "Identifier",
                "name": "a",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 6,
                    "column": 8
                  },
                  "end": {
                    "line": 6,
                    "column": 9
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 6,
                  "column": 8
                },
                "end": {
                  "line": 6,
                  "column": 9
                }
              }
            },
            "decorators": [],
            "loc": {
              "start": {
                "line": 6,
                "column": 5
              },
              "end": {
                "line": 6,
                "column": 6
              }
            }
          },
          "init": null,
          "loc": {
            "start": {
              "line": 6,
              "column": 5
            },
            "end": {
              "line": 6,
              "column": 6
            }
          }
        }
      ],
      "kind": "var",
      "loc": {
        "start": {
          "line": 6,
          "column": 1
        },
        "end": {
          "line": 6,
          "column": 10
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "AssignmentExpression",
        "operator": "=",
        "left": {
          "type": "MemberExpression",
          "object": {
            "type": "Identifier",
            "name": "b",
            "decorators": [],
            "loc": {
              "start": {
                "line": 7,
                "column": 1
              },
              "end": {
                "line": 7,
                "column": 2
              }
            }
          },
          "property": {
            "type": "Identifier",
            "name": "foo",
            "decorators": [],
            "loc": {
              "start": {
                "line": 7,
                "column": 3
              },
              "end": {
                "line": 7,
                "column": 6
              }
            }
          },
          "computed": false,
          "optional": false,
          "loc": {
            "start": {
              "line": 7,
              "column": 1
            },
            "end": {
              "line": 7,
              "column": 6
            }
          }
        },
        "right": {
          "type": "NumberLiteral",
          "value": 2,
          "loc": {
            "start": {
              "line": 7,
              "column": 9
            },
            "end": {
              "line": 7,
              "column": 10
            }
          }
        },
        "loc": {
          "start": {
            "line": 7,
            "column": 1
          },
          "end": {
            "line": 7,
            "column": 10
          }
        }
      },
      "loc": {
        "start": {
          "line": 7,
          "column": 1
        },
        "end": {
          "line": 7,
          "column": 11
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "AssignmentExpression",
        "operator": "=",
        "left": {
          "type": "MemberExpression",
          "object": {
            "type": "Identifier",
            "name": "b",
            "decorators": [],
            "loc": {
              "start": {
                "line": 8,
                "column": 1
              },
              "end": {
                "line": 8,
                "column": 2
              }
            }
          },
          "property": {
            "type": "Identifier",
            "name": "bar",
            "decorators": [],
            "loc": {
              "start": {
                "line": 8,
                "column": 3
              },
              "end": {
                "line": 8,
                "column": 6
              }
            }
          },
          "computed": false,
          "optional": false,
          "loc": {
            "start": {
              "line": 8,
              "column": 1
            },
            "end": {
              "line": 8,
              "column": 6
            }
          }
        },
        "right": {
          "type": "StringLiteral",
          "value": "",
          "loc": {
            "start": {
              "line": 8,
              "column": 9
            },
            "end": {
              "line": 8,
              "column": 14
            }
          }
        },
        "loc": {
          "start": {
            "line": 8,
            "column": 1
          },
          "end": {
            "line": 8,
            "column": 14
          }
        }
      },
      "loc": {
        "start": {
          "line": 8,
          "column": 1
        },
        "end": {
          "line": 8,
          "column": 15
        }
      }
    },
    {
      "type": "ExpressionStatement",
      "expression": {
        "type": "AssignmentExpression",
        "operator": "=",
        "left": {
          "type": "MemberExpression",
          "object": {
            "type": "Identifier",
            "name": "b",
            "decorators": [],
            "loc": {
              "start": {
                "line": 9,
                "column": 1
              },
              "end": {
                "line": 9,
                "column": 2
              }
            }
          },
          "property": {
            "type": "Identifier",
            "name": "foobar",
            "decorators": [],
            "loc": {
              "start": {
                "line": 9,
                "column": 3
              },
              "end": {
                "line": 9,
                "column": 9
              }
            }
          },
          "computed": false,
          "optional": false,
          "loc": {
            "start": {
              "line": 9,
              "column": 1
            },
            "end": {
              "line": 9,
              "column": 9
            }
          }
        },
        "right": {
          "type": "NumberLiteral",
          "value": 2,
          "loc": {
            "start": {
              "line": 9,
              "column": 12
            },
            "end": {
              "line": 9,
              "column": 13
            }
          }
        },
        "loc": {
          "start": {
            "line": 9,
            "column": 1
          },
          "end": {
            "line": 9,
            "column": 13
          }
        }
      },
      "loc": {
        "start": {
          "line": 9,
          "column": 1
        },
        "end": {
          "line": 9,
          "column": 14
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 9,
      "column": 14
    }
  }
}
TypeError: Property 'foobar' does not exist on type 'a'. [memberExpTest_1.ts:9:3]
