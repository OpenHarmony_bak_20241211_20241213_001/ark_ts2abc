/**
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ES2PANDA_IR_TS_TYPE_REFERENCE_H
#define ES2PANDA_IR_TS_TYPE_REFERENCE_H

#include <ir/expression.h>

namespace panda::es2panda::binder {
class ScopeFindResult;
class Variable;
}  // namespace panda::es2panda::binder

namespace panda::es2panda::compiler {
class PandaGen;
}  // namespace panda::es2panda::compiler

namespace panda::es2panda::checker {
class Checker;
class Type;
}  // namespace panda::es2panda::checker

namespace panda::es2panda::ir {

class TSTypeParameterInstantiation;

class TSTypeReference : public Expression {
public:
    explicit TSTypeReference(Expression *typeName, TSTypeParameterInstantiation *typeParams)
        : Expression(AstNodeType::TS_TYPE_REFERENCE), typeName_(typeName), typeParams_(typeParams)
    {
    }

    const TSTypeParameterInstantiation *TypeParams() const
    {
        return typeParams_;
    }

    const Expression *TypeName() const
    {
        return typeName_;
    }

    static checker::Type *ResolveReference(checker::Checker *checker, binder::Variable *var,
                                           const ir::Identifier *refIdent,
                                           const ir::TSTypeParameterInstantiation *typeParams);

    void Iterate(const NodeTraverser &cb) const override;
    void Dump(ir::AstDumper *dumper) const override;
    void Compile([[maybe_unused]] compiler::PandaGen *pg) const override;
    checker::Type *Check([[maybe_unused]] checker::Checker *checker) const override;

private:
    Expression *typeName_;
    TSTypeParameterInstantiation *typeParams_;
};
}  // namespace panda::es2panda::ir

#endif
